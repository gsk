package at.mus.GestureSensitiveControl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import at.mus.recognition.*;
import at.mus.recognition.dtw.AccelerationFactory;
import at.mus.recognition.dtw.DTW;

public class GestureActivity extends Activity implements SensorEventListener {
	private static final NumberFormat codePointFormat = DecimalFormat.getInstance(Locale.ENGLISH);

	// ui elements
	private Button btnFindServer;
	private Button btnCloseApp;
	private Button btnConnect;
	private Button btnDevMessage1;
	private Button btnDevMessage2;
	private Button btnRecognize;
	private TextView lbInformation;
	private TextView lbAcceleration;
	private TextView lbOrientation;
	private TextView lbGyroscope;
	private EditText txtServerIp;

	// logic
	private Connector connector;
	
	// sensor
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private Sensor mOrientation;
	private Sensor mLinearAcceleration;

	private ArrayList<ICoordinate> data = new ArrayList<ICoordinate>();
	private IRecognizer dtw = new DTW();
	private CoordinateFactory coords = new AccelerationFactory();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		connector = new Connector();
		
		setContentView(R.layout.main);

		// get ui controls
		lbInformation = (TextView) findViewById(R.id.lbInformation);
		lbAcceleration = (TextView) findViewById(R.id.lbAcceleration);
		lbOrientation = (TextView) findViewById(R.id.lbOrientation);
		lbGyroscope = (TextView) findViewById(R.id.lbGyroscope);
		txtServerIp = (EditText) findViewById(R.id.txtServerIp);
		btnFindServer = (Button) findViewById(R.id.btnFindServer);
		btnFindServer.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				btnFindServer.setEnabled(false);
				txtServerIp.setText(connector.findServers());
				btnFindServer.setEnabled(true);
			}
		});
		btnCloseApp = (Button) findViewById(R.id.btnCloseApp);
		btnCloseApp.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				connector.close();
				finish();
			}
		});
		btnConnect = (Button) findViewById(R.id.btnConnect);
		btnConnect.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				lbInformation.setText("Trying to connect to Server ...");
				String ip = txtServerIp.getText().toString();
				connector.connectToServer(ip);

				lbInformation.setText("Connected to Server, make gesture !");
				btnDevMessage1.setEnabled(true);
				btnDevMessage2.setEnabled(true);
				btnRecognize.setEnabled(true);
			}
		});
		btnDevMessage1 = (Button) findViewById(R.id.btnSendDevMessag1);
		btnDevMessage1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				connector.send("ForwardGesture");
			}
		});
		btnDevMessage2 = (Button) findViewById(R.id.btnSendDevMessag2);
		btnDevMessage2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				connector.send("BackwardGesture");
			}
		});
		
		btnRecognize = (Button) findViewById(R.id.btnRecognize);
		btnRecognize.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					startGesture();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					endGesture();
				}
				return false;
			}
		});
	}

	protected void onResume() {
		super.onResume();
		if (mSensorManager != null) { // maybe delete this one
			mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
			mSensorManager.registerListener(this, mLinearAcceleration, SensorManager.SENSOR_DELAY_NORMAL);
			mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
		}
	}

	protected void onPause() {
		super.onPause();
		if (mSensorManager != null) {
			mSensorManager.unregisterListener(this);
		}
	}

	private void startGesture() {
		// create sensor manager and accelerometer
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		mLinearAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

		// clear list
		data.clear();

		// register listener
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mLinearAcceleration, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_NORMAL);
	}

	private void endGesture() {
		// deregister listener
		mSensorManager.unregisterListener(this);

		Result r = dtw.recognize(data, Templates.templates, 0, 0, null);
		lbOrientation.setText(r.getTemplate().name + "   " + r.getMatch());
		
		connector.send(r.getTemplate().name);
		
		//this.createVectorListCode(data);
	}

	// will be called if accuracy changes
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	// will be called if a sensor changes its value
	public void onSensorChanged(SensorEvent event) {
		String s = "";
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			s = String.format("Acc.: X:%f Y:%f Z:%f", event.values[0],
					event.values[1], event.values[2]);
			lbAcceleration.setText(s);
		}
		if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
			s = String.format("%s %n Orientation: X:%f Y:%f Z:%f", s,
					event.values[0], event.values[1], event.values[2]);
			lbOrientation.setText(s);
			// mPosRecorder.setOrientation(event.values);
		}
		if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
			s = String.format("LinAcc.: X:%f Y:%f Z:%f", event.values[0],
					event.values[1], event.values[2]);
			lbGyroscope.setText(s);
			data.add(coords.create(event.values));
		}
	}

	@SuppressWarnings("unused")
	private void createVectorListCode(List<ICoordinate> points) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("public static float[][] vectors = { "));

		int cnt = 0;
		for (ICoordinate c : points) {
			sb.append(String.format("{ %1$sf, %2$sf, %3$sf }", codePointFormat.format(c.getX()), 
					codePointFormat.format(c.getY()), 
					codePointFormat.format(c.getZ())));
			++cnt;
			if (cnt < points.size())
				sb.append(", ");
		}
		sb.append("};");

		// send mail
		final Intent emailIntent = new Intent(
				android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { "patrick.ecker@gmail.com" });
		emailIntent.putExtra(android.content.Intent.EXTRA_CC, new String[] {
				"martin.enzelsberger@fh-hagenberg.at", "knittl89@gmail.com" });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				"MUS Testdata - Links Kr�mmung");
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, sb.toString());
		this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
	}
}