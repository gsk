package at.mus.GestureSensitiveControl;

public class PositionRecorder {
	private float azimuth, pitch, roll;
	private float ax, ay, az;
	
	public float x, y, z;

	public void setOrientation(float[] orientationValues) {
		azimuth = orientationValues[0];
		pitch = orientationValues[1];
		roll = orientationValues[2];
	}
	
	public void setAcceleration(float[] accelerationValues) {
		ax = accelerationValues[0];
		ay = accelerationValues[1];
		az = accelerationValues[2];
		calculate();
	}
	
	private void calculate() {
        x =(float) (ax*(Math.cos(roll)*Math.cos(azimuth)+Math.sin(roll)*Math.sin(pitch)*Math.sin(azimuth)) + ay*(Math.cos(pitch)*Math.sin(azimuth)) + az*(-Math.sin(roll)*Math.cos(azimuth)+Math.cos(roll)*Math.sin(pitch)*Math.sin(azimuth)));
        y = (float) (ax*(-Math.cos(roll)*Math.sin(azimuth)+Math.sin(roll)*Math.sin(pitch)*Math.cos(azimuth)) + ay*(Math.cos(pitch)*Math.cos(azimuth)) + az*(Math.sin(roll)*Math.sin(azimuth)+ Math.cos(roll)*Math.sin(pitch)*Math.cos(azimuth)));
        z = (float) (ax*(Math.sin(roll)*Math.cos(pitch)) + ay*(-Math.sin(pitch)) + az*(Math.cos(roll)*Math.cos(pitch)));
        
	}
}
