package at.mus.GestureSensitiveControl;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

public class Connector {
	private static final int UDP_PORT = 45000;
	private static final int TCP_PORT = 35000;
	private Socket clientSocket;
	private DataOutputStream outToServer;

	public String findServers() {
		DatagramPacket pack;
		byte[] buf = new byte[1000];
		String message = "FindServer";
		try {
			InetAddress addr = InetAddress.getByName("255.255.255.255");
			DatagramSocket dgsock = new DatagramSocket();
			pack = new DatagramPacket(message.getBytes(), message.length(), addr, UDP_PORT);
			dgsock.send(pack); // send udp message

			pack = new DatagramPacket(buf, buf.length);
			dgsock.receive(pack);
			String recString = new String(pack.getData(), 0, pack.getLength());
			return recString;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void connectToServer(final String ip) {
		new Thread() {
			public void run() {
				try {
					clientSocket = new Socket(ip, TCP_PORT);
					outToServer = new DataOutputStream(clientSocket.getOutputStream());
					outToServer.writeBytes("Hello Server");
					Thread.sleep(1000);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	public void send(String message) {
		if (outToServer == null)
			throw new IllegalStateException("No connection active, cannot send message");
		try {
			outToServer.writeBytes(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void close(){
		try {
			if (outToServer != null)
				outToServer.close();
			if (clientSocket != null)
				clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}
