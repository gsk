package at.mus.recognition;

public interface ICoordinate {
	float distance(ICoordinate other);
	ICoordinate add(ICoordinate other);
	ICoordinate divide(float f);
	float[] coords();
	void coords(float[] coords);
	
	float getX();
	float getY();
	float getZ();
}
