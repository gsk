package at.mus.recognition.dtw;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.mus.recognition.*;

public class DTW implements IRecognizer {

	@Override
	public Result recognize(List<ICoordinate> rawPoints, List<Template> templates, int n_, double size, ICoordinate origin) {
		int[] score = new int[templates.size()];
		int i = 0;
		int minIndex = 0;

		List<ICoordinate> points = quantize(compress(rawPoints));
		for(Template t : templates) {
			List<ICoordinate> pointList = new ArrayList<ICoordinate>(Arrays.asList(t.points));
			score[i] = this.dist(points, quantize(compress(pointList)));
			if(score[i] < score[minIndex]) minIndex = i;
			++i;
		}

		for(int s : score) {
			System.err.println(s);
		}

		return new Result(templates.get(minIndex), score[minIndex]);
	}

	private int dist(List<ICoordinate> s, List<ICoordinate> t) {
		int m = t.size(), n = s.size();
		int[][] dtw = new int[n][m]; 
		int cost;

		for(int i=0; i < m; ++i) { dtw[0][i] = Integer.MAX_VALUE; }
		for(int i=0; i < n; ++i) { dtw[i][0] = Integer.MAX_VALUE; }
		dtw[0][0] = 0;

		for(int i=1; i < n; ++i) {
			for(int j=1; j < m; ++j) {
				cost = (int)s.get(i).distance(t.get(j));
				dtw[i][j] = cost + Math.min(
						dtw[i-1][j],  // insertion
						Math.min(dtw[i][j-1],  // deletion
								dtw[i-1][j-1])); // match
			}
		}

		return dtw[n-1][m-1];
	}

	private List<ICoordinate> compress(List<ICoordinate> points) {
		List<ICoordinate> compressed = new ArrayList<ICoordinate>();
		int windowSize = 10, stepSize = 6;

		for(int i = 0, l = points.size(); i+windowSize < l; i += stepSize) {
			ICoordinate total = new Acceleration(0, 0, 0);
			for(int j = 0; j < windowSize; ++j) {
				total = total.add(points.get(i+j));
			}
			compressed.add(total.divide(windowSize));
		}

		return compressed;
	}

	private List<ICoordinate> quantize(List<ICoordinate> points) {
		List<ICoordinate> quantized = new ArrayList<ICoordinate>();

		for(ICoordinate coord : points) {
			float[] coords = coord.coords();
			for(float c : coords) { c = quantize(c); }
			quantized.add(new Acceleration(coords));
		}

		return quantized;
	}
	private float quantize(float f) {
		if(f < 0) return -quantizeSymm(f);
		return quantizeSymm(f);
	}

	private float quantizeSymm(float f) {
		final float g = 10f;//9.8f;
		if(f > 2*g) return 16;
		if(f > g) return 11+(int)(5*(f-g)/g); // 11-15
		if(f > 0) return 1+(int)(10*f/g); // 1-10
		return 0;
	}
}
