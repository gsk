package at.mus.recognition.dtw;

import at.mus.recognition.Template;

public class TemplateDTW extends Template {
	
	public TemplateDTW(String name, float[][] points) {
		super(name);
		this.points = new Acceleration[points.length];
		for (int i = 0; i < points.length; i++)
			this.points[i] = new Acceleration((float)points[i][0], (float)points[i][1], (float)points[i][2]);
	}
}
