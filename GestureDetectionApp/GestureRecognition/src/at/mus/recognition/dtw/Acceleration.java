package at.mus.recognition.dtw;

import at.mus.recognition.ICoordinate;

public class Acceleration implements ICoordinate {
	public float x, y, z;

	public Acceleration(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Acceleration(float[] value) {
		this(value[0], value[1], value[2]);
	}

	/**
	 * Returns distance between 2 vectors
	 * @param acc
	 * @return
	 */
	@Override
	public float distance(ICoordinate acc) {
		return (float) Math.sqrt(difference((Acceleration)acc).length());
	}
	
	/**
	 * Returns length of the vector
	 * @return
	 */
	public float length() {
		return (float)Math.sqrt(x*x + y*y + z*z);
	}
	
	/**
	 * Returns difference between 2 vectors
	 * @return
	 */
	public Acceleration difference(Acceleration acc) {
		return new Acceleration(x - acc.x, y - acc.y, z - acc.z);
	}

	@Override
	public ICoordinate add(ICoordinate other) {
		Acceleration acc = (Acceleration)other;
		return new Acceleration(x+acc.x, y+acc.y, z+acc.z);
	}

	@Override
	public ICoordinate divide(float f) {
		return new Acceleration((float)x/f, (float)y/f, (float)z/f);
	}

	@Override
	public float[] coords() {
		return new float[] { x, y, z };
	}

	@Override
	public void coords(float[] coords) {
		x = coords[0];
		y = coords[1];
		z = coords[2];
	}

	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public float getZ() {
		return z;
	}
}
