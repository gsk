package at.mus.recognition.dtw;

import at.mus.recognition.CoordinateFactory;
import at.mus.recognition.ICoordinate;

public class AccelerationFactory extends CoordinateFactory{

	@Override
	public ICoordinate create(float x, float y, float z) {
		return new Acceleration(x, y, z);
	}
	
}
