package at.mus.recognition.ThreeDollar;

import at.mus.recognition.*;

public class Template3Dollar extends Template {
	public Vector[] points;

	public Template3Dollar(String name, float[][] points) {
		super(name);
		this.points = new Vector[points.length];
		for (int i = 0; i < points.length; i++)
			this.points[i] = new Vector(points[i][0], points[i][1], points[i][2]);
	}
}
