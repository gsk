package at.mus.recognition.ThreeDollar;

import java.util.*;

import at.mus.recognition.Cube;
import at.mus.recognition.Template;
import at.mus.recognition.TwoDim.Template2D;

public class Recognizer {
	private static final float Phi = 0.5f * (-1.0f + (float)Math.sqrt(5.0)); // Golden Ratio
	private static final float AngleRange = deg2Rad(45.0f);
	private static final float AnglePrecision = deg2Rad(2.0f);

	/* Step 1: Resample */

	public List<Vector> resample(List<Vector> list, int n) {
		List<Vector> newList = new ArrayList<Vector>(list.size());
		float I = pathLength(list) / (n - 1);
		float D = 0.0f;

		newList.add(list.get(0));
		for (int i = 1; i < list.size(); i++) {
			Vector pi_1 = list.get(i - 1);
			Vector pi = list.get(i);
			double d = pi_1.distance(pi);
			if (D + d >= I) {
				Vector unit = pi.difference(pi_1).unit();
				float dist = I - D;
				Vector q = new Vector(pi_1.x + dist * unit.x,
						pi_1.y + dist * unit.y,	pi_1.z + dist * unit.z);
				newList.add(q);
				list.add(i, q);
				D = 0.0f;
			} else
				D += d;
		}
		// somtimes we fall a rounding-error short of adding the last Acceleration, so
		// add it if so
		if (newList.size() == (n - 1)) {
			Vector last = list.get(list.size() - 1);
			newList.add(new Vector(last));
		}
		return newList;
	}

	private float pathLength(List<Vector> points) {
		float d = 0.0f;
		for (int i = 1; i < points.size(); i++)
			d += points.get(i - 1).distance(points.get(i));
		return d;
	}

	/* Step 2: Rotate */

	public List<Vector> rotateToZero(List<Vector> points) {
		Vector c = centroid(points);
		Vector p0 = points.get(0);
		
		float theta = c.angle(p0);
		Vector axis = p0.orthogonal(c).unit();
		float[][] matrix = createRotationMatrix(axis.values(), theta);
		
		List<Vector> newPoints = new ArrayList<Vector>(points.size());
		for (Vector v : points)
			newPoints.add(rotate(v, matrix));
		return newPoints;
	}

/*	private List<Vector> rotateBy(List<Vector> points, Vector c, double radians) {
		double cos = Math.cos(radians);
		double sin = Math.sin(radians);

		List<Vector> newPoints = new ArrayList<Vector>();
		for (Vector p : points) {
			newPoints.add(new Vector(
					(p.x - c.x) * cos - (p.y - c.y) * sin + c.x, (p.x - c.x)
							* sin + (p.y - c.y) * cos + c.y));
		}
		return newPoints;
	}*/

	private Vector rotate(Vector v, float[][]m) {
		float[] out = { 0.0f, 0.0f, 0.0f };
		for (int i = 0; i < 3; i++) {
			out[i] = v.x * m[i][0] + v.y * m[i][1] + v.z * m[i][2];
		}
		return new Vector(out);
	}
	
	private float[][] createRotationMatrix(float[] axis, float angle) {
		// generate a rotation matrix for rotation along axis with the value theta
		float x = axis[0];
		float y = axis[1];
		float z = axis[2];
		// float angle = (float) theta;
		float[] rx = { (float) (1 + (1 - Math.cos(angle)) * (x * x - 1)),
				(float) ((float) -z * Math.sin(angle) + (1 - Math.cos(angle)) * x * y),
				(float) (y * Math.sin(angle) + (1 - Math.cos(angle)) * x * z) };
		float[] ry = { (float) (z * Math.sin(angle) + (1 - Math.cos(angle)) * x * y), (float) (1 + (1 - Math.cos(angle)) * (y * y - 1)),
				(float) (-x * Math.sin(angle) + (1 - Math.cos(angle)) * y * z) };
		float[] rz = { (float) (-y * Math.sin(angle) + (1 - Math.cos(angle)) * x * z),
				(float) (x * Math.sin(angle) + (1 - Math.cos(angle)) * y * z), (float) (1 + (1 - Math.cos(angle)) * (z * z - 1)) };
		float[][] matrix = { rx, ry, rz };

		return matrix;
	}
	
	private Vector centroid(List<Vector> points) {
		float cx = 0.0f, cy = 0.0f, cz = 0.0f;
		for (Vector p : points) {
			cx += p.x;
			cy += p.y;
			cz += p.z;
		}
		cx /= points.size();
		cy /= points.size();
		cz /= points.size();
		return new Vector(cx, cy, cz);
	}
	
	/* Step 3: Scale */

	public List<Vector> scaleAndTranslateTo(List<Vector> points, float size/*, Vector pt*/) {
		List<Vector> newPoints = new ArrayList<Vector>(points.size());

		// scale
		Cube cube = boundingBox(points);
		for (Vector p : points) {
			newPoints.add(new Vector(
					p.x * (size / cube.width), 
					p.y * (size / cube.height),
					p.z * (size / cube.depth)));
		}

		// translate
		/*Vector c = centroid(newPoints);
		for (Vector p : newPoints) {
			p.x = p.x + pt.x - c.x;
			p.y = p.y + pt.y - c.y;
		}*/
		return newPoints;
	}

	private Cube boundingBox(List<Vector> points) {
		float minX = Float.MAX_VALUE, maxX = Float.MIN_VALUE;
		float minY = Float.MAX_VALUE, maxY = Float.MIN_VALUE;
		float minZ = Float.MAX_VALUE, maxZ = Float.MIN_VALUE;
		for (Vector p : points) {
			if (p.x < minX)
				minX = p.x;
			if (p.y < minY)
				minY = p.y;
			if (p.z < minZ)
				minZ = p.z;
			if (p.x > maxX)
				maxX = p.x;
			if (p.y > maxY)
				maxY = p.y;
			if (p.z > maxZ)
				maxZ = p.z;
		}
		return new Cube(minX, minY, minZ, maxX - minX, maxY - minY, maxZ - minZ);
	}

	/* Step 4: Recognition */
	
	/**
	 * @param rawPoints list of points to recognize
	 * @param templates pre-defined list of templates
	 * @param n number of points (resample)
	 * @param size square size (scaleAndTranslateTo)
	 * @param origin desired origin (scaleAndTranslateTo)
	 */
/*public Result recognize(List<Vector> rawPoints, List<Template2D> templates, int n, float size) {
		List<Vector> points = scaleAndTranslateTo(rotateToZero(resample(rawPoints, n)), size);
		
		Vector c = centroid(points);
		double b = Double.MAX_VALUE, d;
		Template2D tpl = null;
		for (Template2D t : templates) {
			d = distanceAtBestAngle(points, t, c, -AngleRange, +AngleRange, AnglePrecision);
			if (d < b) {
				b = d; // best (least) distance
				tpl = t;
			}
		}
		double halfDiagonal = Math.sqrt(size * size * 2) * 0.5;
		return new Result(tpl, 1.0 - b / halfDiagonal);
	}

	private double distanceAtBestAngle(List<Vector> points, Template t, Vector c, float a, float b, float threshold) {
		float x1 = Phi * a + (1.0f - Phi) * b;
		float f1 = distanceAtAngle(points, t, c, x1);
		float x2 = (1.0f - Phi) * a + Phi * b;
		float f2 = distanceAtAngle(points, t, c, x2);

		while (Math.abs(b - a) > threshold) {
			if (f1 < f2) {
				b = x2;
				x2 = x1;
				f2 = f1;
				x1 = Phi * a + (1.0f - Phi) * b;
				f1 = distanceAtAngle(points, t, c, x1);
			} else {
				a = x1;
				x1 = x2;
				f1 = f2;
				x2 = (1.0f - Phi) * a + Phi * b;
				f2 = distanceAtAngle(points, t, c, x2);
			}
		}
		return Math.min(f1, f2);
	}
*/
	private float distanceAtAngle(List<Vector> points, Template t, Vector c, float angles[]) {
		float dist = Float.MAX_VALUE;
		float a = angles[0];
		float b = angles[1];
		float g = angles[2];

		float[][] matrix = {
				{ (float) (Math.cos(a) * Math.cos(b)), (float) (Math.cos(a) * Math.sin(b) * Math.sin(g) - Math.sin(a) * Math.cos(g)),
						(float) (Math.cos(a) * Math.sin(b) * Math.cos(g) + Math.sin(a) * Math.sin(g)) },
				{ (float) (Math.sin(a) * Math.cos(b)), (float) (Math.sin(a) * Math.sin(b) * Math.sin(g) + Math.cos(a) * Math.cos(g)),
						(float) (Math.sin(a) * Math.sin(b) * Math.cos(g) - Math.cos(a) * Math.sin(g)) },
				{ (float) (-Math.sin(b)), (float) (Math.cos(b) * Math.sin(g)), (float) (Math.cos(b) * Math.cos(g)) } };

		List<Vector> newCandPoints = new ArrayList<Vector>(points.size());
		for (Vector v : points)
			newCandPoints.add(rotate(v, matrix));
		//dist = this.path_distance(newCandPoints, t.points);

		return dist;
	}
	
	public float path_distance(List<Vector> path1, Vector[] path2) { 
		float distance = 0.0f;
		for (int i = 0; i < Math.min(path1.size(), path2.length); i++)
			distance += path1.get(i).distance(path2[i]);
		return distance;
	}

	/* help methods */
	private static float deg2Rad(float d) { 
		return (d * (float)Math.PI / 180.0f); 
	}
	
	/*// not used
	 private static double rad2Deg(double r) { 
		return (r * 180.0 / Math.PI); 
	}*/
	
	public static class Result {
		private Template2D tpl;
		private double match;

		public Result(Template2D tpl, double match) {
			super();
			this.tpl = tpl;
			this.match = match;
		}

		public Template2D getTemplate() {
			return tpl;
		}

		public void setTemplate(Template2D tpl) {
			this.tpl = tpl;
		}

		public double getMatch() {
			return match;
		}

		public void setMatch(double match) {
			this.match = match;
		}
	}
}
