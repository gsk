package at.mus.recognition.ThreeDollar;

import at.mus.recognition.ICoordinate;

public class Vector implements ICoordinate {
	public float x, y, z;

	public Vector(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector(float[] value) {
		this(value[0], value[1], value[2]);
	}

	public Vector(Vector acc) {
		this.x = acc.x;
		this.y = acc.y;
		this.z = acc.z;
	}

	public float[] values() {
		return new float[] { x, y, z };
	}

	/**
	 * Returns distance between 2 vectors
	 * 
	 * @param acc
	 * @return
	 */
	public float distance(Vector acc) {
		return (float) Math.sqrt((x - acc.x) * (x - acc.x) + (y - acc.y) * (y - acc.y) + (z - acc.z) * (z - acc.z));
	}

	/**
	 * Returns length of the vector
	 * 
	 * @return
	 */
	public float length() {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	/**
	 * Returns unit vector
	 * 
	 * @return
	 */
	public final Vector unit() {
		float norm = (float) (1.0 / length());
		return new Vector(norm * x, norm * y, norm * z);
	}

	/**
	 * Returns difference between 2 vectors
	 * 
	 * @return
	 */
	public final Vector difference(Vector v) {
		return new Vector(x - v.x, y - v.y, z - v.z);
	}

	/**
	 * Returns vector orthogonal to (cross-product of) this vector and a
	 * 
	 * @param v
	 * @return
	 */
	public Vector orthogonal(Vector v) {
		return new Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	public final float dot_product(Vector v) {
		return x * v.x + y * v.y + z * v.z;
	}

	public final float norm_dot_product(Vector v) {
		return dot_product(v) / (length() * v.length());
	}

	public final float angle(Vector v) {
		float norm_product = norm_dot_product(v);
		if (norm_product <= 1.0f) {
			float theta = (float) Math.acos(norm_product);
			return theta;
		} else {
			return 0.0f;
		}
	}

	@Override
	public float distance(ICoordinate other) { throw new UnsupportedOperationException();	}

	@Override
	public ICoordinate add(ICoordinate other) { throw new UnsupportedOperationException();	}

	@Override
	public ICoordinate divide(float f) { throw new UnsupportedOperationException();	}

	@Override
	public float[] coords() { throw new UnsupportedOperationException();	}

	@Override
	public void coords(float[] coords) { throw new UnsupportedOperationException();	}
	
	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public float getZ() {
		return z;
	}
}
