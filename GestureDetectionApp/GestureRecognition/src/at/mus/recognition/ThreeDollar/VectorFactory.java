package at.mus.recognition.ThreeDollar;

import at.mus.recognition.CoordinateFactory;
import at.mus.recognition.ICoordinate;

public class VectorFactory extends CoordinateFactory {

	@Override
	public ICoordinate create(float x, float y, float z) {
		return new Vector(x, y, z);
	}

}
