package at.mus.recognition;

public class Cube {
	public float x, y, z, width, height, depth;

	public Cube(float x, float y, float z, float width, float height, float depth) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.depth = depth;
	}
}