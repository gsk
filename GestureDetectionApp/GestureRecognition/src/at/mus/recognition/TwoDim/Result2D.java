package at.mus.recognition.TwoDim;


public class Result2D {
	private Template2D tpl;
	private double match;

	public Result2D(Template2D tpl, double match) {
		super();
		this.tpl = tpl;
		this.match = match;
	}

	public Template2D getTemplate() {
		return tpl;
	}

	public void setTemplate(Template2D tpl) {
		this.tpl = tpl;
	}

	public double getMatch() {
		return match;
	}

	public void setMatch(double match) {
		this.match = match;
	}
}