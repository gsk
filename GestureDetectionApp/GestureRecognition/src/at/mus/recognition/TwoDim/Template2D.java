package at.mus.recognition.TwoDim;


public class Template2D {
	public String name;
	public Point[] points;
	
	public Template2D(String name, double[][] points) {
		this.name = name;
		this.points = new Point[points.length];
		for (int i = 0; i < points.length; i++)
			this.points[i] = new Point(points[i][0], points[i][1]);
	}
}
