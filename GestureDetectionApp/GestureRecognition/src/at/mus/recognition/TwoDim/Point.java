package at.mus.recognition.TwoDim;

public class Point {
	public double x, y;

	public Point() {
		this.x = 0.0;
		this.y = 0.0;
	}

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Point(Point p) {
		this.x = p.x;
		this.y = p.y;
	}

	//@Override
	public float distance(Object p){
		double dx = ((Point)p).x - x;
		double dy = ((Point)p).y - y;
		return (float)Math.sqrt(dx * dx + dy * dy);
	}
}
