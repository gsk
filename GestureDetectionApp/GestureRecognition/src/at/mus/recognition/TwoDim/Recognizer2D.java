package at.mus.recognition.TwoDim;

import java.util.*;

import at.mus.recognition.Rectangle;

public class Recognizer2D {//implements IRecognizer {
	private static final double Phi = 0.5 * (-1.0 + Math.sqrt(5.0)); // Golden Ratio
	private static final double AngleRange = deg2Rad(45.0);
	private static final double AnglePrecision = deg2Rad(2.0);

	/* Step 1: Resample */

	public List<Point> resample(List<Point> points, int n) {
		List<Point> newPoints = new ArrayList<Point>();
		double I = pathLength(points) / (n - 1);
		double D = 0.0;

		newPoints.add(points.get(0));
		for (int i = 1; i < points.size(); i++) {
			Point pi_1 = points.get(i - 1);
			Point pi = points.get(i);
			double d = pi_1.distance(pi);
			if (D + d >= I) {
				Point q = new Point(pi_1.x + ((I - D) / d) * (pi.x - pi_1.x),
						pi_1.y + ((I - D) / d) * (pi.y - pi_1.y));
				newPoints.add(q);
				points.add(i, q);
				D = 0.0;
			} else
				D += d;
		}
		// somtimes we fall a rounding-error short of adding the last point, so
		// add it if so
		if (newPoints.size() == (n - 1)) {
			Point last = points.get(points.size() - 1);
			newPoints.add(new Point(last.x, last.y));
		}
		return newPoints;
	}

	private double pathLength(List<Point> points) {
		double d = 0.0;
		for (int i = 1; i < points.size(); i++)
			d += points.get(i - 1).distance(points.get(i));
		return d;
	}

	/* Step 2: Rotate */

	public List<Point> rotateToZero(List<Point> points) {
		Point c = centroid(points);
		Point p0 = points.get(0);
		double radians = Math.atan2(c.y - p0.y, c.x - p0.x); // indicative angle
		return rotateBy(points, c, radians);
	}

	private List<Point> rotateBy(List<Point> points, Point c, double radians) {
		double cos = Math.cos(radians);
		double sin = Math.sin(radians);

		List<Point> newPoints = new ArrayList<Point>();
		for (Point p : points) {
			newPoints.add(new Point(
					(p.x - c.x) * cos - (p.y - c.y) * sin + c.x, (p.x - c.x)
							* sin + (p.y - c.y) * cos + c.y));
		}
		return newPoints;
	}

	private Point centroid(List<Point> points) {
		double cx = 0.0, cy = 0.0;
		for (Point p : points) {
			cx += p.x;
			cy += p.y;
		}
		cx /= points.size();
		cy /= points.size();
		return new Point(cx, cy);
	}

	/* Step 3: Scale */

	public List<Point> scaleAndTranslateTo(List<Point> points, double size, Point pt) {
		List<Point> newPoints = new ArrayList<Point>();

		// scale
		Rectangle rect = boundingBox(points);
		for (Point p : points) {
			newPoints.add(new Point(p.x * (size / rect.width), p.y
					* (size / rect.height)));
		}

		// translate
		Point c = centroid(newPoints);
		for (Point p : newPoints) {
			p.x = p.x + pt.x - c.x;
			p.y = p.y + pt.y - c.y;
		}
		return newPoints;
	}

	private Rectangle boundingBox(List<Point> points) {
		double minX = Double.MAX_VALUE, maxX = Double.MIN_VALUE;
		double minY = Double.MAX_VALUE, maxY = Double.MIN_VALUE;
		for (Point p : points) {
			if (p.x < minX)
				minX = p.x;
			if (p.y < minY)
				minY = p.y;
			if (p.x > maxX)
				maxX = p.x;
			if (p.y > maxY)
				maxY = p.y;
		}
		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

	/* Step 4: Recognition */
	
	/**
	 * @param rawPoints list of points to recognize
	 * @param templates pre-defined list of templates
	 * @param n number of points (resample)
	 * @param size square size (scaleAndTranslateTo)
	 * @param origin desired origin (scaleAndTranslateTo)
	 */
	//@Override
	public Result2D recognize(List<Point> rawPoints, List<Template2D> templates, int n, double size, Point origin) {
		List<Point> points = scaleAndTranslateTo(rotateToZero(resample(rawPoints, n)), size, origin);
		
		Point c = centroid(points);
		double b = Double.MAX_VALUE, d;
		Template2D tpl = null;
		for (Template2D t : templates) {
			d = distanceAtBestAngle(points, t, c, -AngleRange, +AngleRange, AnglePrecision);
			if (d < b) {
				b = d; // best (least) distance
				tpl = t;
			}
		}
		double halfDiagonal = Math.sqrt(size * size * 2) * 0.5;
		return new Result2D(tpl, 1.0 - b / halfDiagonal);
	}

	private double distanceAtBestAngle(List<Point> points, Template2D t, Point c, double a, double b, double threshold) {
		double x1 = Phi * a + (1.0 - Phi) * b;
		double f1 = distanceAtAngle(points, t, c, x1);
		double x2 = (1.0 - Phi) * a + Phi * b;
		double f2 = distanceAtAngle(points, t, c, x2);

		while (Math.abs(b - a) > threshold) {
			if (f1 < f2) {
				b = x2;
				x2 = x1;
				f2 = f1;
				x1 = Phi * a + (1.0 - Phi) * b;
				f1 = distanceAtAngle(points, t, c, x1);
			} else {
				a = x1;
				x1 = x2;
				f1 = f2;
				x2 = (1.0 - Phi) * a + Phi * b;
				f2 = distanceAtAngle(points, t, c, x2);
			}
		}
		return Math.min(f1, f2);
	}

	private double distanceAtAngle(List<Point> points, Template2D t, Point c,
			double radians) {
		List<Point> newPoints = rotateBy(points, c, radians);

		/* path distance */
		double d = 0.0;
		for (int i = 0; i < newPoints.size(); i++)
			d += newPoints.get(i).distance(t.points[i]);
		return d / newPoints.size();
	}

	/* help methods */
	private static double deg2Rad(double d) { 
		return (d * Math.PI / 180.0); 
	}
}
