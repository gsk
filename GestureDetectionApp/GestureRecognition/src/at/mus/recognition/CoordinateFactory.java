package at.mus.recognition;

public abstract class CoordinateFactory {
	public abstract ICoordinate create(float x, float y, float z);
	
	public ICoordinate create(float[] values) {
		return create(values[0], values[1], values[2]);
	}
}
