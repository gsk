package at.mus.recognition;

import java.util.List;


public interface IRecognizer {
	Result recognize(List<ICoordinate> rawPoints, List<Template> templates, int n, double size, ICoordinate origin);
}
