package at.mus.recognition;

public abstract class Template {
	public String name;
	public ICoordinate[] points;
	
	public Template(String name) {
		this.name = name;
	}
}
