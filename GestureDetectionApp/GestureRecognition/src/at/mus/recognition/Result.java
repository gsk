package at.mus.recognition;


public class Result {
	private Template tpl;
	private double match;

	public Result(Template tpl, double match) {
		super();
		this.tpl = tpl;
		this.match = match;
	}

	public Template getTemplate() {
		return tpl;
	}

	public void setTemplate(Template tpl) {
		this.tpl = tpl;
	}

	public double getMatch() {
		return match;
	}

	public void setMatch(double match) {
		this.match = match;
	}
}