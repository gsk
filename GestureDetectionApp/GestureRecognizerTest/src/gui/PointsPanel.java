package gui;

import javax.swing.JPanel;

import at.mus.recognition.TwoDim.Point;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class PointsPanel extends JPanel {
	/* constants */
	private final int POINT_SIZE = 7;
	
	private String title;
	private List<Point> points;
	private Color col;
	private double offsetX, offsetY;
	
	public PointsPanel(String title, List<Point> points, Color col) {
		this(title, points, col, 0, 0);
	}
	
	public PointsPanel(String title, List<Point> points, Color col, double offsetX, double offsetY) {
		this.title = title;
		this.points = points;
		this.col = col;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.setBackground(Color.white);
	}

	public void update(List<Point> points) {
		this.points = points;
		repaint();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.DARK_GRAY);
		g.drawString(title, 5, 15);
		g.drawRect(0, 0, getWidth()-1, getHeight()-1);
		if (offsetX != 0 || offsetY != 0) {
			g.drawLine(0, (int)offsetY, getWidth(), (int)offsetY);
			g.drawLine((int)offsetX, 0, (int)offsetX, getHeight());
		}
		
		g.setColor(col);
		if (points != null) {
			for (Point p : new ArrayList<Point>(points)) {
				g.drawOval((int)(p.x + offsetX), (int)(p.y + offsetY), POINT_SIZE, POINT_SIZE);
			}
		}
	}
}
