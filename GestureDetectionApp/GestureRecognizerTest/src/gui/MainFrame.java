package gui;

import javax.swing.*;

import at.mus.recognition.TwoDim.*;
import at.mus.recognition.TwoDim.Point;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.List;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements ActionListener {
	/* constants */
	private final int NumPoints = 64;
	private final double SquareSize = 250;
	private final Point Origin = new Point(0,0);
	private static final NumberFormat codePointFormat = DecimalFormat.getInstance(Locale.ENGLISH);
	
	/* UI */
	private PointsPanel rawPanel;
	private PointsPanel resamplePanel;
	private PointsPanel rotationPanel;
	private PointsPanel scaledPanel;
	private JButton btnMakeTemplate;
	private JButton btnRecognize;
	private JTextArea txtCode;
	private JLabel lblMatch;

	/* logic */
	private boolean recording;
	private Thread drawThread;
	private long delay = 10;
	
	/* recognition */
	private Recognizer2D recognizer;
	private List<Point> rawPoints;
	private List<Point> resampledPoints;
	private List<Point> rotatedPoints;
	private List<Point> scaledPoints;

	public MainFrame() {
		super("GestureRecognition");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		setLayout(new BorderLayout(5, 5));

		init();
		initUI();
		drawThread.start();
	}

	private void init() {
		recognizer = new Recognizer2D();
		
		recording = false;
		rawPoints = new ArrayList<Point>();
		resampledPoints = new ArrayList<Point>();
		rotatedPoints = new ArrayList<Point>();
		scaledPoints = new ArrayList<Point>();

		drawThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(delay);
						if (recording && rawPanel.getMousePosition() != null) {
							record(new Point(rawPanel.getMousePosition().getX(),
									rawPanel.getMousePosition().getY()));
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void initUI() {
		/* centerPanel*/
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(2, 2));
		getContentPane().add(centerPanel, BorderLayout.CENTER);
		
		/* rawPanel */
		rawPanel = new PointsPanel("Input", rawPoints, Color.orange);
		rawPanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				if (e.getButton() == MouseEvent.BUTTON1 && !recording)
					startRecording();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				super.mouseReleased(e);
				if (e.getButton() == MouseEvent.BUTTON1 && recording)
					stopRecording();
			}
		});
		centerPanel.add(rawPanel);
		
		/* resamplePanel */
		resamplePanel = new PointsPanel("Resampled", resampledPoints, Color.green);
		centerPanel.add(resamplePanel);
		
		/* rotationPanel */
		rotationPanel = new PointsPanel("Rotated", rotatedPoints, Color.blue);
		centerPanel.add(rotationPanel);
		
		/* scaledPanel: use offset because origin is (0,0) and points may be negative */
		scaledPanel = new PointsPanel("Scaled & Translated", scaledPoints, Color.red, SquareSize, SquareSize);
		centerPanel.add(scaledPanel);
		
		/* bottom panel */
		JPanel botPanel = new JPanel();
		botPanel.setLayout(new GridLayout(1, 2));
		getContentPane().add(botPanel, BorderLayout.SOUTH);
		
		/* code field */
		txtCode = new JTextArea();
		txtCode.setRows(5);
		txtCode.setLineWrap(true);
		botPanel.add(txtCode);

		/* buttonPanel */
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 5));
		getContentPane().add(btnPanel, BorderLayout.NORTH);

		/* buttons */
		btnMakeTemplate = new JButton("Template");
		btnMakeTemplate.addActionListener(this);
		btnPanel.add(btnMakeTemplate);
		
		btnRecognize = new JButton("Recognize");
		btnRecognize.addActionListener(this);
		btnPanel.add(btnRecognize);
		
		/* detection */
		JLabel label = new JLabel("Match:");
		btnPanel.add(label);
		lblMatch = new JLabel("");
		btnPanel.add(lblMatch);
		
	}

	private void startRecording() {
		recording = true;
		rawPoints.clear();
		resampledPoints.clear();
	}

	private void stopRecording() {
		recording = false;
	}

	private void record(at.mus.recognition.TwoDim.Point p) {
		rawPoints.add(p);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				rawPanel.repaint();
			}
		});
	}

	public static void main(String[] args) {
		MainFrame frame = new MainFrame();
		frame.setVisible(true);
	}
	
	private void createPointListCode(List<Point> points) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("public static double[][] points = { "));
		
		int cnt = 0;
		for (Point p : points) {
			sb.append(String.format("{ %1$s, %2$s }", codePointFormat.format(p.x), codePointFormat.format(p.y)));
			++cnt;
			if (cnt < points.size())
				sb.append(", ");
		}
		sb.append("};");
		txtCode.setText(sb.toString());
	}
	
	/*@SuppressWarnings("unused")
	private void createVectorListCode(List<Vector> points) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("public static double[][] vectors = { "));
		
		int cnt = 0;
		for (Vector v : points) {
			sb.append(String.format("{ %1$s, %2$s, %3$s }", codePointFormat.format(v.x), codePointFormat.format(v.y), codePointFormat.format(v.z)));
			++cnt;
			if (cnt < points.size())
				sb.append(", ");
		}
		sb.append("};");
		txtCode.setText(sb.toString());
	}*/


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnMakeTemplate) {
			lblMatch.setText("");
			resampledPoints = recognizer.resample(new ArrayList<Point>(rawPoints), NumPoints);
			resamplePanel.update(resampledPoints);
			
			rotatedPoints = recognizer.rotateToZero(resampledPoints);
			rotationPanel.update(rotatedPoints);
			
			scaledPoints = recognizer.scaleAndTranslateTo(rotatedPoints, SquareSize, Origin);
			scaledPanel.update(scaledPoints);
			
			createPointListCode(scaledPoints);
		} else if (e.getSource() == btnRecognize) {
			lblMatch.setText("");
			
			Result2D result = recognizer.recognize(rawPoints, Templates2D.list, NumPoints, SquareSize, Origin);
			lblMatch.setText(String.format("%1$s (%2$6.2f)", result.getTemplate().name, result.getMatch() * 100));
		} else {
			/* no button recognized */
		}
	}
	
}
