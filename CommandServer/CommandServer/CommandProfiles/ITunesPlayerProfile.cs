﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTunesLib;

namespace CommandServer.CommandProfiles {
  public class ITunesPlayerProfile : CommandProfile {

    // Important:
    // Just open the Properties tab for the assembly in Visual Studio 2010 and set "Embed Interop Types" to "False".
    private iTunesAppClass myiTunes = new iTunesAppClass();

    public ITunesPlayerProfile(Action<string> logError)
      : base(logError) {
      ProfileName = "ITunes";
    }

    public override void RightGesture() {
      try {
        myiTunes.NextTrack();
      } catch (Exception e) {
        LogError(e.Message);
      }
    }

    public override void LeftGesture() {
      try {
        myiTunes.PreviousTrack();
      } catch (Exception e) {
        LogError(e.Message);
      }
    }

    public override void OkGesture() {
      try {
        myiTunes.Play();
      } catch (Exception e) {
        LogError(e.Message);
      }
    }

    public override void CancelGesture() {
      try {
        myiTunes.Pause();
      } catch (Exception e) {
        LogError(e.Message);
      }
    }
  }
}
