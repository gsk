﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CommandServer.CommandProfiles {
  public class WindowsProfile : CommandProfile {
    public WindowsProfile(Action<string> logError)
      : base(logError) {
      ProfileName = "Windows Profile";
    }

    public override void RightGesture() {
      try {
        SendKeys.SendWait("%{TAB}");
      } catch (Exception e) {
        LogError(e.Message);
      }
    }

    public override void LeftGesture() {
      try {
        LockWorkStation();
      } catch (Exception e) {
        LogError(e.Message);
      }
    }

    public override void OkGesture() {
      try {
        Type typeShell = Type.GetTypeFromProgID("Shell.Application");
        object objShell = Activator.CreateInstance(typeShell);
        typeShell.InvokeMember("MinimizeAll", System.Reflection.BindingFlags.InvokeMethod, null, objShell, null);
      } catch (Exception e) {
        LogError(e.Message);
      }
    }

    public override void CancelGesture() {
      try {
        Type typeShell = Type.GetTypeFromProgID("Shell.Application");
        object objShell = Activator.CreateInstance(typeShell);
        typeShell.InvokeMember("UndoMinimizeAll", System.Reflection.BindingFlags.InvokeMethod, null, objShell, null);
      } catch (Exception e) {
        LogError(e.Message);
      }
    }

    [DllImport("user32.dll")]
    public static extern bool LockWorkStation();
  }
}
