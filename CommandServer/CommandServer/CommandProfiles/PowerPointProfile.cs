﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;

namespace CommandServer.CommandProfiles {
  public class PowerPointProfile : CommandProfile {

    public PowerPointProfile(Action<string> logError)
      : base(logError) {
      ProfileName = "Microsoft PowerPoint";
    }

    public override void RightGesture() {
      try {
        var app = (Application)Marshal.GetActiveObject("PowerPoint.Application");
        var presentation = app.ActivePresentation;
        int i = presentation.SlideShowWindow.View.CurrentShowPosition;
        if (i < presentation.Slides.Count)
          presentation.SlideShowWindow.View.GotoSlide(++i, MsoTriState.msoFalse);
      } catch (Exception e) {
        LogError(e.Message);
        LogError(e.StackTrace);
      }
    }

    public override void LeftGesture() {
      try {
        var app = (Application)Marshal.GetActiveObject("PowerPoint.Application");
        var presentation = app.ActivePresentation;
        int i = presentation.SlideShowWindow.View.CurrentShowPosition;
        if (i > 1)
          presentation.SlideShowWindow.View.GotoSlide(--i, MsoTriState.msoFalse);
      } catch (Exception e) {
        LogError(e.Message);
        LogError(e.StackTrace);
      }
    }

    public override void OkGesture() {
      try {
         var app = (Application)Marshal.GetActiveObject("PowerPoint.Application");
        var presentation = app.ActivePresentation;
        presentation.SlideShowWindow.View.First();
      } catch (Exception e) {
        LogError(e.Message);
        LogError(e.StackTrace);
      }
    }

    public override void CancelGesture() {
      try {
        var app = (Application)Marshal.GetActiveObject("PowerPoint.Application");
        var presentation = app.ActivePresentation;
        presentation.SlideShowWindow.View.Exit();
      } catch (Exception e) {
        LogError(e.Message);
        LogError(e.StackTrace);
      }
    }

    
  }
}
