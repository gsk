﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandServer.CommandProfiles {
  public abstract class CommandProfile {
    public CommandProfile(Action<string> logger) {
      this.LogError = logger;
    }

    protected Action<string> LogError { get; set; }
    protected string ProfileName { get; set; }

    public abstract void RightGesture();
    public abstract void LeftGesture();
    public abstract void OkGesture();
    public abstract void CancelGesture();

    public override string ToString() {
      return ProfileName;
    }
  }
}