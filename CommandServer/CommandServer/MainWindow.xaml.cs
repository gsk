﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CommandServer.CommandProfiles;

namespace CommandServer {
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window {

    #region members
    // gui
    public ObservableCollection<HistoryItem> historyList { get; set; }
    public ObservableCollection<CommandProfile> commandProfileList { get; set; }
    public Action<string> LogErrorAction;

    // communication
    private const int UDP_PORT = 45000;
    private const int TCP_PORT = 35000;
    private Thread setupUdpServerThread;
    private Socket udpSocket;
    private TcpListener tcpListener;
    private Thread listenThread;
    private CommandProfile currentlySelectedProfile = null;
    private string ipMessage = "0.0.0.0";

    #endregion

    public MainWindow() {
      InitializeComponent();

      // init
      LogErrorAction = LogError;
      historyList = new ObservableCollection<HistoryItem>();
      commandProfileList = new ObservableCollection<CommandProfile>() {
                                                                        new ITunesPlayerProfile(LogError), 
                                                                        new PowerPointProfile(LogError), 
                                                                        new WindowsProfile(LogError)
                                                                      };
      lbHistory.ItemsSource = historyList;
      cbCommandProfiles.ItemsSource = commandProfileList;
      cbCommandProfiles.SelectedIndex = 0;
      currentlySelectedProfile = (CommandProfile)cbCommandProfiles.SelectedItem;


      IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

      foreach (IPAddress localIP in localIPs) {
        if (!IPAddress.IsLoopback(localIP)
          && !localIP.IsIPv6LinkLocal
          && !localIP.IsIPv6Multicast
          && !localIP.IsIPv6SiteLocal && !localIP.IsIPv6Teredo) {
          ipMessage = localIP.ToString();
        }
      }
    }

    #region events

    private void btnStartServer_Click(object sender, RoutedEventArgs e) {
      try {
        // start udp server
        setupUdpServerThread = new Thread(SetUpUdpServer);
        setupUdpServerThread.Start();
        Log("UDP Server started");

        // start tcp server
        tcpListener = new TcpListener(IPAddress.Any, TCP_PORT);
        listenThread = new Thread(ListenForClients);
        listenThread.Start();
        Log(string.Format("TCP Listener ({0}) created and server thread started", tcpListener.LocalEndpoint.ToString()));

        // disable gui elements
        btnStartServer.IsEnabled = false;
        btnStopServer.IsEnabled = true;
      } catch (Exception exc) {
        LogError(exc.Message);
      }
    }

    private void btnStopServer_Click(object sender, RoutedEventArgs e) {
      try {
        udpSocket.Close();
        tcpListener.Stop();
        Log("TCP Listener stopped");
        btnStartServer.IsEnabled = true;
        btnStopServer.IsEnabled = false;
      } catch (Exception exc) {
        LogError(exc.Message);
      }
    }

    private void btnDevGesture1_Click(object sender, RoutedEventArgs e) {
      var profile = (CommandProfile)cbCommandProfiles.SelectedItem;
      if (profile == null) {
        this.Log("No profile selected");
        return;
      }
      profile.RightGesture();
    }

    private void btnDevGesture2_Click(object sender, RoutedEventArgs e) {
      var profile = (CommandProfile)cbCommandProfiles.SelectedItem;
      if (profile == null) {
        this.Log("No profile selected");
        return;
      }
      profile.LeftGesture();
    }

    private void btnDevGesture3_Click(object sender, RoutedEventArgs e) {
      var profile = (CommandProfile)cbCommandProfiles.SelectedItem;
      if (profile == null) {
        this.Log("No profile selected");
        return;
      }
      profile.OkGesture();
    }

    private void btnDevGesture4_Click(object sender, RoutedEventArgs e) {
      var profile = (CommandProfile)cbCommandProfiles.SelectedItem;
      if (profile == null) {
        this.Log("No profile selected");
        return;
      }
      profile.CancelGesture();
    }

    private void cbCommandProfiles_SelectionChanged(object sender, SelectionChangedEventArgs e) {
      currentlySelectedProfile = (CommandProfile)cbCommandProfiles.SelectedItem;
    }

    #endregion

    #region methods

    private void SetUpUdpServer() {
      try {
        int recv;
        byte[] data = new byte[1024];
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, UDP_PORT);
        udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        udpSocket.Bind(ipep);

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)(sender);
        recv = udpSocket.ReceiveFrom(data, ref Remote);

        Log(string.Format("Message received from {0}: {1}", Remote, Encoding.ASCII.GetString(data, 0, recv)));

        data = Encoding.ASCII.GetBytes(ipMessage);
        udpSocket.SendTo(data, data.Length, SocketFlags.None, Remote);
        while (true) {
          data = new byte[1024];
          recv = udpSocket.ReceiveFrom(data, ref Remote);
          Log(string.Format("Message received from {0}: {1}", Remote, Encoding.ASCII.GetString(data, 0, recv)));

          data = Encoding.ASCII.GetBytes(ipMessage);
          udpSocket.SendTo(data, data.Length, SocketFlags.None, Remote);
        }
      } catch (Exception exc) {
        LogError(exc.Message);
        LogError(exc.StackTrace);
      }
    }

    /// <summary>
    /// listens for clients
    /// </summary>
    private void ListenForClients() {
      this.tcpListener.Start();
      Log("TCP Listener started");
      try {
        // set up
        while (true) {
          //blocks until a client has connected to the server
          TcpClient client = this.tcpListener.AcceptTcpClient();
          Log("Client connected to server");

          //create a thread to handle communication with connected client
          Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
          clientThread.Start(client);
        }
      } catch (Exception exc) {
        LogError(exc.Message);
      }
    }

    /// <summary>
    /// handles client communication
    /// </summary>
    /// <param name="client">connected client</param>
    private void HandleClientComm(object client) {
      Log("Handle Client Connection");
      TcpClient tcpClient = (TcpClient)client;
      NetworkStream clientStream = tcpClient.GetStream();

      byte[] message = new byte[4096];
      int bytesRead;

      while (true) {
        bytesRead = 0;

        try {
          //blocks until a client sends a message
          bytesRead = clientStream.Read(message, 0, 4096);
        } catch (Exception exc) {
          //a socket error has occured 
          Log(exc.Message);
          break;
        }

        if (bytesRead == 0) {
          //the client has disconnected from the server
          LogError("Client has disconnectd from server");
          break;
        }

        //message has successfully been received
        ASCIIEncoding encoder = new ASCIIEncoding();
        string received = encoder.GetString(message, 0, bytesRead);

        if (!String.IsNullOrEmpty(received)) {
          //Log("Client send message: " + received);
          ProcessMessage(received);
        }
      }

      tcpClient.Close();
    }

    private void ProcessMessage(string s) {
      if (currentlySelectedProfile == null) {
        this.Log("No profile selected");
        return;
      }
      switch (s) {
        case "left":
          currentlySelectedProfile.LeftGesture();
          Log("Gesture: LEFT", 2);
          break;
        case "right":
          currentlySelectedProfile.RightGesture();
          Log("Gesture: RIGHT", 2);
          break;
        case "ok":
          currentlySelectedProfile.OkGesture();
          Log("Gesture: OK", 2);
          break;
        case "cancel":
        //  currentlySelectedProfile.CancelGesture();
          Log("Gesture: CANCEL", 2);
          break;
        case "throw":
          currentlySelectedProfile.CancelGesture();
          Log("Gesture: THROW", 2);
          break;
        case "GestureNotRecognized":
          Log("Gesture not recognized on client",1);
          break;
        case "Error":
          LogError("Error occured on Smartphone");
          break;
        default:
          Log("Couldn't process message from client: " + s);
          break;
      }
    }

    private void Log(string msg, int type = 0) {
      Dispatcher.Invoke(
        new Action(delegate() {
        var lines = msg.Split('\n');
        foreach (var l in lines) {
          historyList.Add(new HistoryItem() { Message = l, Error = type});
        }
      }));
    }

    private void LogError(string msg) {
      Dispatcher.Invoke(
        new Action(delegate() {
        var lines = msg.Split('\n');
        foreach (var l in lines) {
          historyList.Add(new HistoryItem() { Message = l, Error = 1 });
        }
      }));
    }

    #endregion

    #region additional classes

    public class HistoryItem {
      public string Message { get; set; }
      public DateTime LogTime { get; private set; }
      public int Error { get; set; }

      public HistoryItem() {
        LogTime = DateTime.Now;
        Error = 0;
      }

      public override string ToString() {
        return string.Format("{0, -10} : {1}", LogTime.ToLongTimeString(), Message);
      }
    }

    #endregion


  }
}